
[![pipeline status](https://gitlab.sorbonne-paris-nord.fr/11931512/jsau-desktop/badges/master/pipeline.svg)](https://gitlab.sorbonne-paris-nord.fr/11931512/jsau-desktop/-/commits/master)
[![coverage report](https://gitlab.sorbonne-paris-nord.fr/11931512/jsau-desktop/badges/master/coverage.svg)](https://gitlab.sorbonne-paris-nord.fr/11931512/jsau-desktop/-/commits/master)


The vue app resources have been added to the repository so any one can build the project without having access to the source. In a real project these files shouldn't be added to the repository, instread we include the source code of these files (The vue application).


Rami ALZEBAK  
NE: 11931512
