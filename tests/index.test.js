
describe("TEST ELECTRON ", () => {
    it("SHOULD RUN ", () => {
        let app;
        expect(() => {
            app = require("../index");
        }).not.toThrow();
        expect(app.quit()).not.toThrow();
    });
})